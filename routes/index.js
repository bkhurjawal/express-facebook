var express = require('express');
var Sequelize = require('sequelize');
var bodyParser = require('body-parser');
var session = require('express-session');
var router = express.Router();
var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
router.use(session({
    secret: 'silly kat',
    resave: false,
    saveUninitialized: true
}));
var flash = require('connect-flash');
router.use(flash());


/* GET home page. */
var sequelize = new Sequelize('facebook', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql'


});
var comments = sequelize.define('commentUsers', {
    comment: Sequelize.STRING
});
router.get('/', function (req, res, next) {
    error = req.flash('error');
    res.render('index', {title: 'Facebook', error: error});
});
router.get('/home', function (req, res, next) {
    if(req.session.name == undefined){
        res.redirect('/');
    }
    var users = req.session.users;
    if(users == undefined){
        users = [];
    }
    comments.findAll().then(function (comments) {
        res.render('welcome', {title: 'Bhavya', name: req.session.name, comments: comments, users: users });
    });


});
router.get('/register', function (req, res, next) {
    res.render('register', {title: 'Register'});
});


var register = sequelize.define('register', {
    name: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING
});


router.post('/register', function (req, resp) {
    var name = req.body.name;
    var email = req.body.email;
    var pass = req.body.password;
    sequelize.sync().then(function () {
        return register.create({
            name: name,
            email: email,
            password: pass
        })

    });
    resp.send('created item');
});
router.post('/', function (req, resp) {
    register.findOne({
            where: {
                email: req.body.email
            }
        })
        .then(function (user) {
            if (user.password == req.body.password) {
                //user.name = req.session.name;
                req.session.name = user;
                console.log('this will be username', req.session.name);
                resp.redirect('/home');
            } else {
                req.flash('error', 'Wrong Password');
                resp.redirect('/');
            }
        })
        .catch(function (a) {
            req.flash('error', 'Invalid credentials. Kindly check or sign up.');
            resp.redirect('/');
        });

});

router.post('/users', function (req, resp) {
    register.findAll().then(function (users) {
        req.session.users = users;
        resp.redirect('/home');
    }).catch(function (a) {
        resp.redirect('/home');
        console.log(a);
    })
});

router.post('/home', function (req, resp) {

    var comment = req.body.comment;

    sequelize.sync().then(function () {
        return comments.create({
            comment: comment
        })

    });
    resp.redirect('/home');
});

router.get('/user/:id', function(req, resp){
    register.findOne({
        where: {
            id: req.params.id
        }
    }).then(function(user){
        user.destroy();
        register.findAll().then(function(users){
            req.session.users = users;
            resp.redirect('/home');
        });
    })
});


module.exports = router;
