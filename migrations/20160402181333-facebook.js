'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {

    queryInterface.createTable(
        'register',
        {
          id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
          },

          name: Sequelize.STRING,
          email: Sequelize.STRING,
          password: Sequelize.STRING
        }
    );
      queryInterface.createTable(
          'commentUsers',
          {
              id: {
                  type: Sequelize.INTEGER,
                  primaryKey: true,
                  autoIncrement: true
              },
              createdAt: {
                  type: Sequelize.DATE
              },
              updatedAt: {
                  type: Sequelize.DATE
              },

              comment: Sequelize.STRING

          }
      )
  },

  down: function (queryInterface, Sequelize) {

  }
};
